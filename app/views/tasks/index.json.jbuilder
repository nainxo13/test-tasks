json.array!(@tasks) do |task|
  json.extract! task, :id, :title, :contents
  json.url task_url(task, format: :json)
end
